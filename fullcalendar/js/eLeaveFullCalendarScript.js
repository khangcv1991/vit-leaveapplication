var ele = [];
var listUrl = "../_vti_bin/listdata.svc/";
$(document).ready(function () {
    Retreive();
});

function Retreive() {
    var listUrl = "../_vti_bin/ListData.svc/GovernmentReportingCalendar";
    $.ajax({
        url: listUrl,
        type: "GET",
        data: {
            $select: "Title,Description,StartTime,EndTime,AllDayEvent,Recurrence,Id"
        },
        headers: {
            accept: "application/json;odata=verbose"
        },
        success: function (data) {
            $.each(data.d.results, function (i) {
                currObj = this;
                var fADE = currObj.AllDayEvent;
                if (fADE != null) {
                    if (fADE == 0) {
                        thisADE = false
                    } else thisADE = true;
                }
                var thisID = currObj.Id;
                var thisTitle = currObj.Title;
                var thisRecurrence = currObj.Recurrence;
                var thisDesc = currObj.Description;
                var x = new Date(parseInt(currObj.StartTime.substr(6)));
                var y = new Date(parseInt(currObj.EndTime.substr(6)));
                ele.push({
                    title: currObj.Title,
                    id: currObj.Id,
                    start: x,
                    description: currObj.Description,
                    end: y,
                    allDay: thisADE,
                });
            });
            BindCalendar();
        }
    });
}

function BindCalendar() {
    var calendarioDiv = $('#calendar');
    var fullCalendar = calendarioDiv.fullCalendar({
        events: ele,
        error: function () {
            alert('Error');
        },
        editable: false,
        firstDay: 0,
        monthNames: ['JANUARY', 'FEBRUARY',
            'MARCH', 'APRIL', 'MAY',
            'JUNE', 'JULY', 'AUGUST', 'SEPTEMBER',
            'OCTOBER', 'NOVEMBER', 'DECEMBER'
        ],
        dayNames: ['Sunday', 'Monday', 'Tuesday',
            'Wednesday', 'Thursday', 'Friday', 'Saturday'
        ],
        dayNamesShort: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
        allDay: true
    });
}