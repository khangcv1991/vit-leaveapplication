﻿
(function () {

})();
(function () {
    'use strict';
    var LEAVE_APPLICATION_FIELDS = [
        {"id": 'ID-leave', 'name': 'ID', 'required': false, 'error_mess': '' },
        {"id": 'EmployeeEmail-leave','name': 'EmployeeEmail', 'required': true, 'error_mess': 'email is required' },
        {"id": 'EmployeeSurname-leave','name': 'EmployeeSurname', 'required': true, 'error_mess': 'surname is required' },
        {"id": 'FirstName-leave','name': 'FirstName', 'required': false , 'error_mess': 'first name is required'},
        {"id": 'EmployeeID-leave','name': 'EmployeeID', 'required': false , 'error_mess': ''},
        {"id": 'Designation-leave', 'name': 'Designation', 'required': false , 'error_mess': ''},
        {"id": 'ReportTo-leave', 'name': 'ReportTo', 'required': true, 'error_mess': 'Report to is required' },
        {"id": 'LeaveType-leave', 'name': 'LeaveType', 'required': true , 'error_mess': ''},
        {"id": 'PayrollCode-leave', 'name': 'PayrollCode', 'required': false , 'error_mess': ''},
        {"id": 'LeaveCategory-leave', 'name': 'LeaveCategory', 'required': false , 'error_mess': ''},
        {"id": 'StartDate-leave', 'name': 'StartDate', 'required': true , 'error_mess': 'startdate is required'},
        {"id": 'ReturnDate-leave', 'name': 'ReturnDate', 'required': true , 'error_mess': 'returndate is required'},
        {"id": 'TotalDays-leave', 'name': 'TotalDays', 'required': false , 'error_mess': ''},
        {"id": 'ActualLeaveChecked-leave', 'name': 'ActualLeaveChecked', 'required': false , 'error_mess': ''},
        {"id": 'ActualLeave-leave', 'name': 'ActualLeave', 'required': false , 'error_mess': ''},
        {"id": 'Status-leave', 'name': 'Status', 'required': false, 'error_mess': '' },
        {"id": 'RejectionReason-leave', 'name': 'RejectionReason', 'required': false , 'error_mess': ''},
        {"id": 'Remarks-leave', 'name': 'Remarks', 'required': false , 'error_mess': ''},
        {"id": 'PRCODE-leave', 'name': 'PRCODE', 'required': false , 'error_mess': ''},
    
    ];
    var app = angular.module('SharePointOnlineDirectives', ['ngMaterial']);
    app.directive('spoLeaveapplication', function ($compile) {
        var scripts = document.getElementsByTagName("script")
        var currentScriptPath = scripts[scripts.length - 1].src;

        return {
            restrict: 'EA', //element
            scope: {
                tenant: '='
            },
            //templateUrl: 'https://localhost:44326/scripts/Directives/LeaveApplication/LeaveApplication.html',
            templateUrl: 'https://sharepointapps.blob.core.windows.net/scripts/directives/leaveapplication/LeaveApplication.aspx',
            replace: true,
            //require: 'ngModel',
            link: function ($scope, elem, attr, ctrl) {
                console.debug($scope);
            },
            controller: Controller
        };
    });
    Controller.$inject = ['$scope', 'SharePointOnlineService', '$timeout', 'ListService', '$q', 'LeaveApplicationService', 'modalService'];
    function Controller($scope, SharePointOnlineService, $timeout, ListService, $q, LeaveApplicationService, modalService) {

         //Working hours per day
         var W_HOUR = 7.6;

        var vm = this;
        var searchData = [];
        // Set the cache key
        var wpId = SharePointOnlineService.GetURLParameters("wpId");
        var cacheKey = 'VIT_LeaveApplication_' + wpId
        var userProfile = undefined;
        $scope.stage = {
            view: '',
            tab: ''
        };
       
        var main_managers = [];
        var is_line_manager = false;
        $scope.leave_application_status = {
            'pending':{ 'user': { 'value' :'Pending' }, 'lineManager':{ 'value' :"Pending Line Manager's Approval" }, 'mainManager':{ 'value' :"Pending Final Approval" } },
            'rejected' :{ 'user': { 'value' :'Declined' }, 'lineManager':{ 'value' :'Declined by Line Manager' }, 'mainManager':{ 'value' :'Declined by Main Manager' } },
            'draft' :{ 'user': { 'value' :'Draft' }, 'lineManager':{ 'value' :'Draft' }, 'mainManager':{ 'value' :'Draft' } },
            'withdraw' :{ 'user': { 'value' :'Withdraw' }, 'lineManager':{ 'value' :'Withdraw' }, 'mainManager':{ 'value' :'Withdraw' } },
            'cancel' :{ 'user': { 'value' :'Cancel' }, 'lineManager':{ 'value' :'Cancel' }, 'mainManager':{ 'value' :'Cancel' } } ,
            'approve' :{ 'user': { 'value' :'Approved' }, 'lineManager':{ 'value' :'Approved' }, 'mainManager':{ 'value' :'Approved' } }
        }

        $scope.applications = [];
        $scope.selectedLeaveApplication = {};
        $scope.selectedLeaveApplication.selectedManager = undefined;
        $scope.selectedLeaveApplication.LeaveType = undefined;
        $scope.selectedLeaveApplication.PayrollCode = undefined;
        $scope.selectedLeaveApplication.enable_leave_category = false;
        $scope.selectedLeaveApplication.SupportingFiles = undefined;
        $scope.selectedLeaveApplication.RejectionReason = undefined;
        $scope.selectedLeaveApplication.Withdraw = undefined;
        $scope.selectedLeaveApplication.Cancel = undefined;
        $scope.selectedLeaveApplication.LeaveCategory = 1;
        $scope.selectedLeaveApplication.PublicHolidays = 0;
        $scope.leave_type = LEAVE_TYPE_PAYROLL_CODE;
        $scope.payroll_code = [];

        $scope.LeaveApplicationData = [];
        $scope.FilterLeaveApplicationData = [];
        $scope.title = 'Base Controller';
        $scope.username = _spPageContextInfo.userDisplayName;

        $scope.managers = [];
        $scope.SearchText = "*sharepoint*";
        $scope.ShowSpinner = false;

        $scope.stage.view = SharePointOnlineService.GetURLParameters("View");


        function _ShowValidationErrors(err) {
            if (err) {
                if (err.ExceptionAsString && err.ExceptionAsString != null) {
                    $("#validationErrors").text(err.ExceptionAsString);
                }
                if (err.Message && err.Message != null) {
                    $("#validationErrors").text(err.Message);
                }
            }
        }
        //map selected item to selectedApplication
        $scope.mapItemToSelApplication = function (item) {
            $scope.selectedLeaveApplication = item;

        }

       


        //constructor
        init();
        function init() {
            //enable checkbox
            $scope.selectedLeaveApplication.ActualLeaveChecked = true;
            var hostUrl = SharePointOnlineService.GetHostWebUrl();
            var appUrl = SharePointOnlineService.GetAppWebUrl();
            var manageUrl = appUrl + "/_api/SP.AppContextSite(@target)/web/sitegroups/getbyname('Staff Leave Manager')/users?@target=%27" + hostUrl + "%27";
            var main_manageUrl = appUrl + "/_api/SP.AppContextSite(@target)/web/sitegroups/getbyname('Staff Leave Main Managers')/users?@target=%27" + hostUrl + "%27";
            // hide error message 
            $("#error-message").hide();
            // load line managers
            var promise1 = ListService.GetListByTitle(manageUrl);
            var promise2 = ListService.GetListByTitle(main_manageUrl);
            var promise3 = SharePointOnlineService.LoadUserProfile();

            $q.all([promise1, promise2, promise3])
                .then(function (data) {
                    console.log(data[0], data[1], data[2]);

                    $scope.managers = data[0];//line managers
                    main_managers = data[1];//main managers
                    userProfile = data[2].userProfileProperties //current user profile
                    //load user leave applications
                    $scope.refreshLeaveApplication_Click();

                    //check if current user in line manager group
                    $scope.managers.forEach(function (item) {
                        if (item.Email == userProfile.WorkEmail) {
                            $scope.managers = main_managers;
                            is_line_manager = true;
                        }

                    });

                });

        }

        function ClearCache() {
            $scope.SearchResults = [];
            SharePointOnlineService.forceCacheDeletion();
        }

        $scope.ClearCacheAndSearch = function (event) {
            if (event != null) {
                event.preventDefault();
            }
            ClearCache();
        }
		
        $scope.filterData = function ($event, filter) {
            $event.preventDefault();
            $scope.stage.tab = filter;
            $scope.FilterLeaveApplicationData = [];

            $scope.LeaveApplicationData.forEach(function (item) {
                if (item.Status == filter) {
                    $scope.FilterLeaveApplicationData.push(item);
                }
                //add cancel - widthdraw
                // if (filter ==  $scope.leave_application_status.cancel.user.value && item.Status ==  $scope.leave_application_status.cancel.user.value) {
                //     $scope.FilterLeaveApplicationData.push(item);

                // }

                if (filter ==  $scope.leave_application_status.cancel.user.value && item.Status ==  $scope.leave_application_status.withdraw.user.value) {
                    $scope.FilterLeaveApplicationData.push(item);

                }
                // pending
                if (filter ==  $scope.leave_application_status.pending.user.value) {
                    if (item.Status.includes( $scope.leave_application_status.pending.user.value)) {
                        $scope.FilterLeaveApplicationData.push(item);

                    }
                  
                }
                //rejected
                if(filter ==  $scope.leave_application_status.rejected.user.value){
                    if (item.Status.includes( $scope.leave_application_status.rejected.user.value)) {
                        $scope.FilterLeaveApplicationData.push(item);

                    }
                }
              
            });
            console.log($scope.FilterLeaveApplicationData);

        }
        $scope.ActualLeaveToggle = function (event) {
            document.getElementById("inpActualLeave").readOnly = !event.target.checked;
            document.getElementById("inpActualLeave").focus();
        }

        $scope.deleteLeaveApplication_Click = function (data) {
            var modalOptions = {
                closeButtonText: 'Cancel',
                actionButtonText: 'Delete selected Leave Application form',
                headerText: 'Delete ' + " the selected application " + '?',
                bodyText: 'Are you sure you want to delete this application?'
            };

            modalService.showModal({}, modalOptions).then(function (result) {
                if (result == 'ok')
                    LeaveApplicationService.LeaveApplication_DeleteLeaveData(data).then(function (data) {
                        //load application data
                        $scope.refreshLeaveApplication_Click();

                    });
            });


        }
        $scope.editLeaveApplication_Click = function (data) {
            
            LEAVE_APPLICATION_FIELDS.forEach(function (item) {
                if (item.required == true) {
                        jQuery("#" + item.id).removeClass("has-error");
                        jQuery("#" + item.id + " input").removeClass("has-error");
                }
            });
            $scope.selectedLeaveApplication = data;
            $scope.selectedLeaveApplication.ActualLeaveChecked = true;
            $scope.leave_type.forEach(function (item) {
                if (data.PayrollCode == item.leave_type_code) {
                    $scope.selectedLeaveApplication.LeaveType = item.leave_type_code;
                    $scope.selectedLeaveApplication.PayrollCode = item.leave_type_code;
                    $scope.selectedLeaveApplication.enable_leave_category = item.enable_leave_category;
                    return;
                }
                if (data.LeaveType == item.leave_type_text) {
                    $scope.selectedLeaveApplication.LeaveType = item.leave_type_code;
                    $scope.selectedLeaveApplication.PayrollCode = item.leave_type_code;
                    $scope.selectedLeaveApplication.enable_leave_category = item.enable_leave_category;
                    return;
                }
            });


            $('#modalLeaveApplication').modal('show');
        }

        $scope.CancelLeaveApplication_Click = function (data) {
                   
                    LeaveApplicationService.LeaveApplication_UpdateLeaveData(data).then(function (success) {
                        var inputEmail = null;
                        //load application data
                        $scope.refreshLeaveApplication_Click();
                    }, function (err) {
                       console.log(err);
                       $scope.refreshLeaveApplication_Click();
                    });

             

        }

        $scope.WithdrawLeaveApplication_Click = function (data) {

            //$scope.selectedLeaveApplication = data;
                    data.Status =  $scope.leave_application_status.withdraw.user.value;
                    LeaveApplicationService.LeaveApplication_UpdateLeaveData(data).then(function (success) {
                        var inputEmail = null;
                        //load application data
                        $scope.refreshLeaveApplication_Click();
                    }, function (err) {
                        console.log(err);
                        $scope.refreshLeaveApplication_Click();
                    });
                


        }
        $scope.refreshLeaveApplication_Click = function () {
            $scope.selectedLeaveApplication.ActualLeaveChecked = true;
            loadLeaveApplication();
            jQuery("#inpReject").removeClass("has-error");
            jQuery("#inpRejectMess").hide();
            
            $("#userTabs li").each(function(){$(this).removeClass("active")}); $("#userTabs li").first().next().addClass("active");
            $("#mainmanagerTabs li").each(function(){$(this).removeClass("active")}); $("#mainmanagerTabs li").first().addClass("active");
            $("#linemanagerTabs li").each(function(){$(this).removeClass("active")}); $("#linemanagerTabs li").first().addClass("active");
            
        }
        $scope.newLeaveApplication_Click = function () {
            $scope.selectedLeaveApplication.ActualLeaveChecked = true;
            // hide error message 
            $("#error-message").hide();

            //$scope.selectedLeaveApplication.ID = undefined;
            if (userProfile == null || userProfile == undefined) {
                $('#modalLeaveApplication').modal('show');
                return;
            }
            $scope.selectedLeaveApplication.ID = undefined;
            $scope.selectedLeaveApplication.EmployeeEmail = userProfile.UserName;
            $scope.selectedLeaveApplication.EmployeeSurname = userProfile.LastName;
            $scope.selectedLeaveApplication.EmployeeFirstname = userProfile.FirstName;
            $scope.selectedLeaveApplication.EmployeeID = userProfile.EmployeeId;
            $scope.selectedLeaveApplication.Department = userProfile.Department;
            $scope.selectedLeaveApplication.Designation = userProfile.Title;
            $scope.selectedLeaveApplication.DiscussedWithManager = false;
            $scope.selectedLeaveApplication.SufficientLeaves = false;

            LEAVE_APPLICATION_FIELDS.forEach(function (item) {
                if (item.required == true) {
                        jQuery("#" + item.id).removeClass("has-error");
                        jQuery("#" + item.id + " input").removeClass("has-error");
                }
            });
            $('#modalLeaveApplication').modal('show');

        }

        $scope.ClearFile = function () {
            document.getElementById("inpFile").value = "";
        }

        $scope.SaveLeaveApplication = function () {
            if($scope.selectedLeaveApplication.DiscussedWithManager == false) {
                $scope.selectedLeaveApplication.DiscussedWithManager = 'No';
            }else{
                $scope.selectedLeaveApplication.DiscussedWithManager = 'Yes';
            }
            if($scope.selectedLeaveApplication.SufficientLeaves == false) {
                $scope.selectedLeaveApplication.SufficientLeaves = 'No';
            }else{
                $scope.selectedLeaveApplication.SufficientLeaves = 'Yes';
            }
            $scope.selectedLeaveApplication.Status = $scope.leave_application_status.draft.user.value;
            var modalOptions = {
                closeButtonText: 'Cancel',
                actionButtonText: 'OK',
                headerText: 'Save ' + " the selected application " + '',
                bodyText: undefined
            };


            if ($scope.selectedLeaveApplication.ID !== undefined) {
                //attach document
                attachDocument();
                //end
                //If leaveApplication is  exist, update selected leave application
                LeaveApplicationService.LeaveApplication_UpdateLeaveData($scope.selectedLeaveApplication).then(function (success) {
                    //load application data
                    $scope.refreshLeaveApplication_Click();
                }, function (err) {
                    $scope.selectedLeaveApplication.Status =  $scope.leave_application_status.draft.user.value;
                    console.log(err);
                });
            } else {
                //If leaveApplication is not exist, create a new leave application
                LeaveApplicationService.LeaveApplication_SaveOrCreateData($scope.selectedLeaveApplication).then(function (success) {

                    console.log(success);
                    $scope.selectedLeaveApplication.ID = success.$2_0.get_properties().Id;
                    //attach document
                    attachDocument();
                    //end
                    $scope.refreshLeaveApplication_Click();

                }, function (err) {
                    $scope.selectedLeaveApplication.Status =  $scope.leave_application_status.draft.user.value;
                    console.log(err);
                });
            }
            //files = $scope.selectedLeaveApplication.SupportingFiles;
            $('#modalLeaveApplication').modal('hide');
        }
        $scope.SubmitLeaveApplication = function () {
           
            if($scope.selectedLeaveApplication.DiscussedWithManager == false) {
                $scope.selectedLeaveApplication.DiscussedWithManager = 'No';
            }else{
                $scope.selectedLeaveApplication.DiscussedWithManager = 'Yes';
            }
            if($scope.selectedLeaveApplication.SufficientLeaves == false) {
                $scope.selectedLeaveApplication.SufficientLeaves = 'No';
            }else{
                $scope.selectedLeaveApplication.SufficientLeaves = 'Yes';
            }

            var modalOptions = {
                closeButtonText: 'Cancel',
                actionButtonText: 'OK',
                headerText: 'Submit ' + " the selected application " + '',
                bodyText: undefined
            };
            modalOptions.bodyText = "Do you want to submit this application? ";
            modalService.showModal({}, modalOptions).then(function (result) {
                if (result == 'ok') {
                    var errs = validateLeaveApplication($scope.selectedLeaveApplication);
                    if (errs.length > 0) {
                        $('#modalLeaveApplication').modal('show');
                        return;
                    }
                    if (is_line_manager == true)
                            $scope.selectedLeaveApplication.Status =  $scope.leave_application_status.pending.mainManager.value;
                    else
                            $scope.selectedLeaveApplication.Status =  $scope.leave_application_status.pending.lineManager.value;
                            
                    if ($scope.selectedLeaveApplication.ID !== null && $scope.selectedLeaveApplication.ID !== undefined) {
                        //attach document
                        attachDocument();
                        //end
                        LeaveApplicationService.LeaveApplication_UpdateLeaveData($scope.selectedLeaveApplication).then(function (success) {
                           
                            
                            $scope.refreshLeaveApplication_Click();

                        }, function (err) {
                            console.log(err);
                        });
                    }
                    else {
                        var errs = validateLeaveApplication($scope.selectedLeaveApplication);
                        if (errs.length > 0) {
                            $('#modalLeaveApplication').modal('show');
                            return;
                        }

                        LeaveApplicationService.LeaveApplication_SaveOrCreateData($scope.selectedLeaveApplication).then(function (success) {
                             
                            //load application data
                            $scope.refreshLeaveApplication_Click();
                            //attach document
                            $scope.selectedLeaveApplication.ID = success.$2_0.get_properties().Id;
                            attachDocument();
                            //end

                        }, function (err) {
                            $scope.selectedLeaveApplication.Status =  $scope.leave_application_status.draft.user.value;
                            console.log(err);

                        });
                    }
                }
            });

            // files = $scope.selectedLeaveApplication.SupportingFiles;

        }



        $scope.RejectLeaveApplication = function () {
            //validate for rejected reason
            if($scope.selectedLeaveApplication.RejectionReason == null ||$scope.selectedLeaveApplication.RejectionReason == "" || $scope.selectedLeaveApplication.RejectionReason == undefined){
                jQuery("#inpReject").addClass("has-error");
                jQuery("#inpRejectMess").show();
                return;
            }else{
                jQuery("#inpReject").removeClass("has-error");
                jQuery("#inpRejectMess").hide();
            }

            //if line manager
            if($scope.stage.view == 'ManagerView'){
                $scope.selectedLeaveApplication.Status =  $scope.leave_application_status.rejected.lineManager.value;
            }else{
                $scope.selectedLeaveApplication.Status =  $scope.leave_application_status.rejected.mainManager.value;
            }
            //close modal form 
            $('#modalRejectLeaveApplication').modal('hide');
            
            LeaveApplicationService.LeaveApplication_UpdateLeaveData($scope.selectedLeaveApplication).then(function (success) {
                var inputEmail = null;
                //load application data
                $scope.refreshLeaveApplication_Click();
                modalOptions.bodyText = "Application has been  rejected successfully";
                modalService.showModal({}, modalOptions);
            }, function (err) {

                modalOptions.bodyText = "Application has been not rejected successfully";
                modalService.showModal({}, modalOptions);
            });
        }
        $scope.MainManagerApproveLeaveApplication = function (data) {

            var modalOptions = {
                closeButtonText: 'Cancel',
                actionButtonText: 'Approve selected Leave Application ',
                headerText: 'Approve ' + " the selected application " + '?',
                bodyText: 'Are you sure you want to approve this application?'
            };

            modalService.showModal({}, modalOptions).then(function (result) {
                data.Status =  $scope.leave_application_status.approve.mainManager.value;
                if (result == 'ok'){
                    LeaveApplicationService.LeaveApplication_UpdateLeaveData(data).then(function (success) {
                        var inputEmail = null;
                        //load application data
                        $scope.refreshLeaveApplication_Click();

                    }, function (err) {
                        console.log(err);
                    });
                }
            });

        }

        $scope.ApproveLeaveApplication = function (data) {


            var modalOptions = {
                closeButtonText: 'Cancel',
                actionButtonText: 'Approve selected Leave Application ',
                headerText: 'Approve ' + " the selected application " + '?',
                bodyText: 'Are you sure you want to approve this application?'
            };

            modalService.showModal({}, modalOptions).then(function (result) {
                data.Status =  $scope.leave_application_status.pending.mainManager.value;
                if (result == 'ok')
                    LeaveApplicationService.LeaveApplication_UpdateLeaveData(data).then(function (success) {
                        var inputEmail = null;
                        //load application data
                        $scope.refreshLeaveApplication_Click();

                     
                    }, function (err) {
                        console.log(err);
                    });
            });

        }



        function loadLeaveApplication() {
            var inputEmail = null;
            inputEmail = userProfile.WorkEmail;
            if ($scope.stage.view == 'UserView') {
                inputEmail = userProfile.WorkEmail;
                loadLeaveApplicationByUserType(inputEmail, USER_TYPE.user);
            }
            else if ($scope.stage.view == 'ManagerView') {
                loadLeaveApplicationByUserType(inputEmail, USER_TYPE.lineManager);
            }
            else if ($scope.stage.view == 'MainManagerView') {
                loadLeaveApplicationByUserType(inputEmail, USER_TYPE.mainManager);
            }
        }
        //Load Leave Application
        // 1 : user
        // 2 : lineManager
        // 3 : mainManager

        function loadLeaveApplicationByUserType(inputEmail, userType) {

            LeaveApplicationService.LeaveApplication_LoadUserData(inputEmail, userType).then(function (data) {

                $scope.LeaveApplicationData = data;
                $scope.FilterLeaveApplicationData = [];
                //draft status by default
                $scope.LeaveApplicationData.forEach(function (item) {

                    if ($scope.stage.view == 'UserView') {
                        if (item.Status ==  $scope.leave_application_status.pending.lineManager.value) {
                            $scope.FilterLeaveApplicationData.push(item);
                        }
                        $scope.stage.tab =  $scope.leave_application_status.pending.user.value;

                    }
                    if ($scope.stage.view == 'ManagerView') {
                        if (item.Status ==  $scope.leave_application_status.pending.lineManager.value) {
                            $scope.FilterLeaveApplicationData.push(item);
                        }
                        $scope.stage.tab =  $scope.leave_application_status.pending.lineManager.value;
                    }

                    if ($scope.stage.view == 'MainManagerView') {
                        if (item.Status ==  $scope.leave_application_status.pending.mainManager.value) {
                            $scope.FilterLeaveApplicationData.push(item);
                        }
                        $scope.stage.tab =  $scope.leave_application_status.pending.mainManager.value;
                    }

                });

            })
        }
        //https://stackoverflow.com/questions/7342957/how-do-you-round-to-1-decimal-place-in-javascript
        function round(value, precision) {
            var multiplier = Math.pow(10, precision || 0);
            return Math.round(value * multiplier) / multiplier;
        }

        //https://stackoverflow.com/questions/28949911/what-does-this-format-means-t000000-000z
        function dateDifference(start, end) {

            // Copy date objects so don't modify originals
            var s = new moment(start, "DD/MM/YYYY");
            var e = new moment(end, "DD/MM/YYYY");

            // Get the difference in whole days
            var totalDays = e.diff(s, 'days') + 1;

            // Get the difference in whole weeks
            var wholeWeeks = totalDays / 7 | 0;

            // Estimate business days as number of whole weeks * 5
            var days = totalDays - wholeWeeks * 2;
            //one week 
            if (s.isoWeekday() >   e.isoWeekday() && wholeWeeks * 7 < totalDays){
                days = days - 2;
            }   
            // one week - last day is sunday
            if (e.isoWeekday() == 7 && wholeWeeks * 7 < totalDays ){
                days = days - 2;
            }
            // one week - last day is Saturday
            if (e.isoWeekday() == 6 && wholeWeeks * 7 < totalDays ) {
                days = days - 1;
            }
            if (days < 0)
                return 0;

            return days;
        }

        //JQuery code for Leave Application


       
        //Attachment start
        function attachDocument() {
            
            var process = false;
            var fileInput = $("#inpFile");
          
            if (fileInput.length < 0 || fileInput[0] == undefined ||typeof(fileInput[0].files) == 'undefined')
                return;
            if ($scope.selectedLeaveApplication.ID == null || fileInput[0].files.length == 0)
                return;


            LeaveApplicationService.LeaveApplication_AddAttachedData(fileInput, $scope.selectedLeaveApplication.ID);

        }
        
        
        //Attachment end
        $scope.$watch('selectedLeaveApplication.LeaveCategory', function () {
            if (parseInt($scope.selectedLeaveApplication.LeaveCategory)  == 2) {
                // $scope.selectedLeaveApplication.enable_leave_category = false;
                jQuery("#enable_leave_attachment").hide();
            }else{
                jQuery("#enable_leave_attachment").show();
            }

        });
         //Actual leave 
         $scope.$watch('selectedLeaveApplication.ActualLeave', function () {
           
            var reg = new RegExp('^[-,0-9,.,0-9]+$'); 
            $("#error-message").hide();
            if(reg.test($scope.selectedLeaveApplication.ActualLeave ) == false){
                $("#error-message").show();
                $("#error-message").html("the input must be number");
                return;
            }

            if($scope.selectedLeaveApplication.ActualLeave < 0){
                $("#error-message").show();
                $("#error-message").html("number must not be negative");
                return;
            }

            if (round($scope.selectedLeaveApplication.TotalDays * W_HOUR, 1 )< $scope.selectedLeaveApplication.ActualLeave   ) {
                /// alert("incorrect");
                $("#error-message").show();
                $("#error-message").html("The input must be a valid number based on total days");
                return;
            } 
            if ($scope.selectedLeaveApplication.TotalDays > 1 &&  (round($scope.selectedLeaveApplication.TotalDays  - 1, 1) * W_HOUR) > $scope.selectedLeaveApplication.ActualLeave ){
                $("#error-message").show();
                $("#error-message").html("The input must be a valid number based on total days");
                return;
            }
            $("#error-message").hide();
            

        });
        //Leave Type and PayCode
        $scope.$watch('selectedLeaveApplication.LeaveType', function () {
            try {
                $scope.leave_type.forEach(function (item) {
                    if ($scope.selectedLeaveApplication.LeaveType == item.leave_type_code) {
                        $scope.selectedLeaveApplication.PayrollCode = item.leave_type_code;
                        $scope.selectedLeaveApplication.enable_leave_category = item.enable_leave_category;
                        return;
                    }
                });

            } catch (ex) {
                console.log(ex);
            }
        });
         //validate attachement on changes
         $scope.vailidateAttachement = function($event){
            var fileInput = $("#inpFile");
            var modalOptions = {
                closeButtonText: 'Cancel',
                actionButtonText: 'OK',
                headerText: 'Error  ' + " " + '',
                bodyText: undefined
            };
            $(fileInput).change(function () {
                    var fileExtension = ['jpeg', 'jpg', 'png','pdf', 'docx', 'doc'];
                    if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
                        modalOptions.bodyText = "This file type is not accepted. Accepted file types are jpeg, jpg, png, pdf, docx, doc";
                        modalService.showModal({}, modalOptions)
                        $(this).val("");
                    }
                    if($(this).files != undefined && ($(this).files.length != undefined || $(this).files.length > 0) && $(this).files[0].size/1024/1024 > 2){
                        alert("File size exceeded. Please upload file less than 2MB");
                        modalOptions.bodyText = "File size exceeded. Please upload file less than 2MB";
                        
                        $(this).val("");
                    }
            });

            
            
        }

        //vailidate errors on form
        function validateLeaveApplication(leaveApplication) {
            var errs = [];
            //validate for actual leave and leave
            if (round($scope.selectedLeaveApplication.TotalDays * W_HOUR,1) < $scope.selectedLeaveApplication.ActualLeave)
                errs.push("actual leave is greater than total days * W_HOUR");
            LEAVE_APPLICATION_FIELDS.forEach(function (item) {
                if (item.required == true) {
                    if (leaveApplication[item.name] == null || leaveApplication[item.name] == undefined) {
                        errs.push(item.error_mess);
                        jQuery("#" + item.id).addClass("has-error");
                        // jQuery("#" + item.id + " input").addClass("has-error");
                    }
                    else{
                        jQuery("#" + item.id).removeClass("has-error");
                        // jQuery("#" + item.id + " input").removeClass("has-error");
                    }
                }
            });
            //validate attached file
            var fileInput = $("#inpFile");
            if ($scope.selectedLeaveApplication.enable_leave_category && $scope.selectedLeaveApplication.LeaveCategory == 1) {
                if ($(fileInput).val() == "" || $(fileInput).val() == null || $(fileInput).val() == undefined){
                    jQuery("label[for=inpFile]" ).addClass("has-error");
                    errs.push("need to attache document");
                }else{
                    jQuery("label[for=inpFile]" ).removeClass("has-error");
                }
            }
            //validate startdate
            // $scope.selectedLeaveApplication.StartDate
            // if ($scope.selectedLeaveApplication.StartDate) {
            //     if ($(fileInput).val() == "" || $(fileInput).val() == null || $(fileInput).val() == undefined){
            //         jQuery("#inpStartDateMsg" ).display();
            //         errs.push("startdate is invalid");
            //     }else{
            //         jQuery("#inpStartDateMsg" ).hide();
            //     }
            // }
            return errs;

        }

        //caluculate start day and last day and set restricted return date
        $scope.$watch('[selectedLeaveApplication.StartDate ,selectedLeaveApplication.ReturnDate,selectedLeaveApplication.PublicHolidays]', function () {
            $scope.selectedLeaveApplication.TotalDays = dateDifference($scope.selectedLeaveApplication.StartDate, $scope.selectedLeaveApplication.ReturnDate) - $scope.selectedLeaveApplication.PublicHolidays;
            if($scope.selectedLeaveApplication.TotalDays < 0)
                $scope.selectedLeaveApplication.TotalDays = 0;
            $scope.selectedLeaveApplication.ActualLeave = round($scope.selectedLeaveApplication.TotalDays * W_HOUR,1);
        });

        

        $("#ppReportsTo").typeahead({
            source: LeaveApplicationService.LeaveApplication_Get_Approvers(),
            //autoSelect: trueFFF
        });

        //active selected tab
        $('#userTabs a').click(function (e) {
            e.preventDefault()
            $(this).tab('show');
        });
        $('#mainmanagerTabs a').click(function (e) {
            e.preventDefault()
            $(this).tab('show');
        });
        $('#linemanagerTabs a').click(function (e) {
            e.preventDefault()
            $(this).tab('show');
        });
      
        //datetimepicker for start and end date
        $scope.$watch('[selectedLeaveApplication.StartDate]', function () {
            $scope.selectedLeaveApplication.TotalDays = dateDifference($scope.selectedLeaveApplication.StartDate, $scope.selectedLeaveApplication.ReturnDate);
            jQuery("#inpReturnDate").val("");
            if($scope.selectedLeaveApplication.StartDate !== null || $scope.selectedLeaveApplication.StartDate !== undefined)
                jQuery("#inpReturnDate").datepicker('setStartDate', $scope.selectedLeaveApplication.StartDate);
                
        });
        // jQuery("#inpStartDate").datepicker({ format: 'dd/mm/yyyy', startDate: new Date() });
        // jQuery("#inpReturnDate").datepicker({ format: 'dd/mm/yyyy', startDate: new Date() });
        jQuery("#inpStartDate").datepicker({ format: 'dd/mm/yyyy' , daysOfWeekDisabled: [0,6]});
        jQuery("#inpReturnDate").datepicker({ format: 'dd/mm/yyyy', daysOfWeekDisabled: [0,6] }); 
        jQuery("#inpStartDate").prop("readonly", true);
        jQuery("#inpReturnDate").prop("readonly", true);

    }
})();